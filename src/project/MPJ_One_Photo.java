package project;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import mpi.MPI;

public class MPJ_One_Photo {
	
	static BufferedImage img=null;
	static  BufferedImage returned=null;
	static File output_file1=null;
	static File received =null;
	static int [][] kernel= new int[3][3];
	
	public MPJ_One_Photo(BufferedImage img, int[][] kernel) {
		this.img=img;
		System.arraycopy(kernel, 0, this.kernel, 0, kernel.length);
	}
	
	public static void main(String[] args) {
		MPI.Init(args);
		int rank = MPI.COMM_WORLD.Rank();
		int tag = 100;
		int tag2 = 200;
		int dest;
		if(rank==1) {   //sending data
			dest =0; //0 process			
				Object[] imageToSend = new Object[1];
				imageToSend[0] = Kernel_Image.transferToRGB(img);  //ArrayList<int[][]> rgb
				MPI.COMM_WORLD.Send(imageToSend, 0, 1, MPI.OBJECT, dest, tag);
				
				
				Object[] imageToRecv = new Object[1];
				MPI.COMM_WORLD.Recv(imageToRecv, 0, 1, MPI.OBJECT, dest, tag2);
				
				received = (File) imageToRecv[0];
		}
		else {   //receiving data
			dest=1;
			Object[] imageToRecv = new Object[1];						
			MPI.COMM_WORLD.Recv(imageToRecv, 0, 1, MPI.OBJECT, dest, tag);  
			ArrayList<int[][]> tempp = (ArrayList<int[][]>) imageToRecv[0];
			BufferedImage backImg = Kernel_Image.backToImg(tempp, img);
			output_file1 = new File("gui/dist.jpg");
			try {
				ImageIO.write(Kernel_Image.sequential(backImg, kernel), "jpg", output_file1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			//send back
			File[] processedImageToSend = new File[1];
			processedImageToSend[0] = output_file1;
			MPI.COMM_WORLD.Send(processedImageToSend, 0, 1, MPI.OBJECT, dest, tag2);
		}
		MPI.Finalize();		
	} 
    
	
	public static BufferedImage MPI_Image() {
		try {
			returned=ImageIO.read(output_file1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returned;
	}

}


