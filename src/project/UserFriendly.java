package project;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;


public class UserFriendly implements ActionListener {
	
	JLabel lbl1, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8,lbl9,lbl20;
	JRadioButton r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11;
	JFrame frame1;
	JPanel panel;
	JButton btn, btn2;
	Integer m;
	Integer n;
	Integer k;
	ImageIcon icon, icon2;
	BufferedImage newImg;
	BufferedImage bi;
	String method;
	int temp[][]; 
	long time;
	GridBagConstraints gc;
	int[][] identity = { { 0, 0, 0 }, { 0, 1, 0 }, { 0, 0, 0 } };
	int[][] edgeDetection1 ={ { 1, 0, -1 }, { 0, 0, 0 }, { -1, 0, 1 } };
	int[][] edgeDetection2 ={ { 0, -1, 0 }, { -1, 4, -1 }, { 0, -1, 0 } };
	int[][] edgeDetection3 ={ { -1, -1, -1 }, { -1, 8, -1 }, { -1, -1, -1 } };
	int[][] sharpen ={ { 0, -1, 0 }, { -1, 5, -1 }, { 0, -1, 0 } };
	int[][] boxBlur = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
	int[][] gaussian1 = { { 1, 2, 1 }, { 2, 4, 2 }, { 1, 2, 1 } };
	int[][] gaussian2 = { { 1, 4, 6, 4, 1 }, { 4, 16, 24, 16, 4 }, { 6, 24, 36, 24, 6 }, { 4, 16, 24, 16, 4 }, { 1, 4, 6, 4, 1 } };
	
	public UserFriendly() {
		frame1 = new JFrame("Kernel Image Processing");
		frame1.setSize(500, 800);
		frame1.setLocationRelativeTo(null); 
		frame1.setVisible(true);
		frame1.setDefaultCloseOperation(frame1.EXIT_ON_CLOSE);
		frame1.setResizable(false);
		frame1.setIconImage(newImg);
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		panel.setBackground(new Color(229,251,229));
		
		//Border innerBorder = BorderFactory.createMatteBorder(5, 5, 5, 5, new Color(28, 21, 79));
		//Border outerBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
		//panel.setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));
		
	
		
		btn2 =new JButton("Continue");
		btn2.addActionListener(this);
		
        gc = new GridBagConstraints();
		
        
        lbl1 = new JLabel("KERNEL IMAGE PROCESSING", SwingConstants.CENTER);
		lbl1.setFont(new Font("Comic Sans MS", Font.BOLD, 20));
        gc.weighty = 0.0;
        //gc.gridwidth = 2;
        gc.gridx = 0;
        gc.gridy = 0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.PAGE_START;
		panel.add(lbl1, gc);
		
		lbl5 = new JLabel("Original image", SwingConstants.CENTER);
		lbl5.setFont(new Font("Comic Sans MS", Font.BOLD, 17));
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.CENTER;
		icon = new ImageIcon("originals/original.png"); 
		lbl3 = new JLabel(icon);
		JPanel original = new JPanel();
		original.add(lbl5);
		original.add(lbl3);
		//gc.gridwidth = 2;
		gc.gridx=0;
		gc.gridy = 1;
		gc.weightx = 1;
		gc.weighty = 0.1;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.CENTER;
		panel.add(original, gc);
		
		lbl2 = new JLabel("Choose kernel: ");
		lbl2.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		gc.gridx = 0;
		gc.gridy = 2;
		gc.weightx = 1;
		gc.weighty = 0.1;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.CENTER;
		panel.add(lbl2, gc);
		
		
		ButtonGroup btnG = new ButtonGroup();
		r1=new JRadioButton("identity");
		r1.addActionListener(this);
		r2=new JRadioButton("edge detection 1");   
		r2.addActionListener(this);
		r3=new JRadioButton("edge detection 2");   
		r3.addActionListener(this);
		r4=new JRadioButton("edge detection 3");
		r4.addActionListener(this);
		JPanel rButtons = new JPanel(); //uses FlowLayout by default
		btnG.add(r1);
		btnG.add(r2);
	    btnG.add(r3);
	    btnG.add(r4);
	    rButtons.add(r1);
	    rButtons.add(r2);
	    rButtons.add(r3);
	    rButtons.add(r4);
		gc.gridx = 0;
		gc.gridy = 3;
		gc.weightx = 0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.CENTER;
		panel.add(rButtons, gc);
		
		r5=new JRadioButton("sharpen");    
		r5.addActionListener(this);
		r6=new JRadioButton("box blur");
		r6.addActionListener(this);
		r7=new JRadioButton("gaussian blur 1");
		r7.addActionListener(this);
		r8=new JRadioButton("gaussian blur 2");
		r8.addActionListener(this);
		btnG.add(r5);
		btnG.add(r6);
	    btnG.add(r7);
	    btnG.add(r8);
		JPanel rButtons2 = new JPanel(); //uses FlowLayout by default
	    rButtons2.add(r5);
	    rButtons2.add(r6);
	    rButtons2.add(r7);
	    rButtons2.add(r8);
	    gc.gridx = 0;
		gc.gridy = 4;
		gc.weightx = 0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.CENTER;
		panel.add(rButtons2, gc);
		
		lbl6 = new JLabel("Method: ", SwingConstants.CENTER);
		lbl6.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		gc.gridx=0;
		gc.gridy = 5;
		gc.weightx = 0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.CENTER;
		panel.add(lbl6, gc);

		ButtonGroup btnG2 = new ButtonGroup();
		r9=new JRadioButton("Sequental");    
		r9.addActionListener(this);
		r10=new JRadioButton("Parallel");  
		r10.addActionListener(this);
		/*r11=new JRadioButton("MPI"); 
		r11.addActionListener(this);*/
		btnG2.add(r9);
		btnG2.add(r10);
		//btnG2.add(r11);
		JPanel rButtons3 = new JPanel(); //uses FlowLayout by default
	    rButtons3.add(r9);
	    rButtons3.add(r10);
	    //rButtons3.add(r11);
	    gc.gridx = 0;
		gc.gridy = 6;
		gc.weightx = 0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.CENTER;
		panel.add(rButtons3, gc);
		
		btn = new JButton("Process");
		btn.addActionListener(this);
		gc.gridx=0;
		gc.gridy = 7;
		gc.weightx = 0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.CENTER;
		panel.add(btn, gc);
		
		
		
		
		/*ImageIcon icon2 = new ImageIcon("src/Untitled-3.png"); 
		lbl4 = new JLabel(icon2);
		gc.gridx=0;
		gc.gridy = 6;
		gc.weightx = 1;
		gc.weighty = 0.1;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_END;
		panel.add(lbl4, gc);
		
		gc.gridx=1;
		gc.gridy = 6;
		gc.weightx = 1;
		gc.weighty = 0.1;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_START;
		panel.add(btn2, gc);*/
		
		
		
		frame1.add(panel);
		
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==r1) {
			temp=new int[3][3];
			System.arraycopy(identity, 0, temp, 0, identity.length);
		}
		if (e.getSource()==r2) {
			temp=new int[3][3];
			System.arraycopy(edgeDetection1, 0, temp, 0, edgeDetection1.length);
		}
		if (e.getSource()==r3) {
			temp=new int[3][3];
			System.arraycopy(edgeDetection2, 0, temp, 0, edgeDetection2.length);
		}
		if (e.getSource()==r4) {
			temp=new int[3][3];
			System.arraycopy(edgeDetection3, 0, temp, 0, edgeDetection3.length);
		}
		if (e.getSource()==r5) {
			temp=new int[3][3];
			System.arraycopy(sharpen, 0, temp, 0, sharpen.length);
		}
		if (e.getSource()==r6) {
			temp=new int[3][3];
			System.arraycopy(boxBlur, 0, temp, 0, boxBlur.length);
		}
		if (e.getSource()==r7) {
			temp=new int[3][3];
			System.arraycopy(gaussian1, 0, temp, 0, gaussian1.length);
		}
		if (e.getSource()==r8) {
			temp=new int[5][5];
			System.arraycopy(gaussian2, 0, temp, 0, gaussian2.length);
		}
		if (e.getSource()==r9) {
			method="seq";
		}
		if (e.getSource()==r10) {
			method="par";
		}
		if (e.getSource()==r11) {
			method="mpi";
		}
		if (e.getSource()==btn) {
			bi = new BufferedImage(
			icon.getIconWidth(),
			icon.getIconHeight(),
		    BufferedImage.TYPE_INT_RGB);
			Graphics g = bi.createGraphics();// paint the Icon to the BufferedImage.
			icon.paintIcon(null, g, 0,0);
			g.dispose();
			if(method==null || temp==null) JOptionPane.showMessageDialog(frame1, "Input not valid");
			if(method=="seq") {
				long startSeq = System.currentTimeMillis();
				newImg = Kernel_Image.sequential(bi, temp);
				long endSeq = System.currentTimeMillis();
				time= endSeq-startSeq;
				System.out.println("seq");
			}
			else if (method=="par") {
				long startPar = System.currentTimeMillis();
				newImg = Kernel_Image.parallel(bi, temp);
				long endPar = System.currentTimeMillis();
				time= endPar-startPar;
				System.out.println("par");
			}
			/*else if (method=="mpi") {
				System.out.println("mpi");
			}*/
			icon2 = new ImageIcon(newImg);
			lbl7 = new JLabel("Processed image", SwingConstants.CENTER);
			lbl7.setFont(new Font("Comic Sans MS", Font.BOLD, 17));
			lbl8 = new JLabel(icon2);
			JPanel processed = new JPanel();
			processed.add(lbl7);
			processed.add(lbl8);
			gc.gridx=0;
			gc.gridy = 8;
			gc.weightx = 0;
			gc.fill = GridBagConstraints.NONE;
			gc.anchor = GridBagConstraints.CENTER;
			panel.add(processed, gc);
			lbl9=new JLabel("Time: "+time+"miliseconds", SwingConstants.CENTER);
			lbl9.setFont(new Font("Comic Sans MS", Font.BOLD, 17));
			lbl9.setOpaque(true);
			gc.gridx=0;
			gc.gridy = 9;
			gc.weightx = 0;
			gc.fill = GridBagConstraints.NONE;
			gc.anchor = GridBagConstraints.CENTER;
			panel.add(lbl9, gc);
			frame1.add(panel);
			frame1.setIconImage(newImg);
			frame1.revalidate();
		}
		
		
	}

}
