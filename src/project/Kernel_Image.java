/*Instructions:
	Pick a kernel from terminal;
    Run;
    Refresh folder "processed" in src;
    All images from folder "originals" are saved in "processed" with the filter you chose as kernel
*/
package project;
//import mpi.*;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.imageio.ImageIO;

public class Kernel_Image {

	static final File dir = new File("originals");
	static File dir2 = new File("processed");
	static final String[] EXTENSIONS = new String[] { "jpg", "png" };  // array of supported extensions 
	static final FilenameFilter IMAGE_FILTER = new FilenameFilter() {  // filter to identify images based on their extensions
		@Override
		public boolean accept(final File dir, final String name) {
			for (final String ext : EXTENSIONS) {
				if (name.endsWith("." + ext)) {
					return (true);
				}
			}
			return (false);
		}
	};

	public static void main(String[] args) {

		Scanner sc= new Scanner(System.in);
		final File[] files = dir2.listFiles();
		for (int j = 0; j < files.length; j++) {
			files[j].delete();                     //empty folder of proccesed imgs before starting
		}
		//kernels
		int[][] identity = { { 0, 0, 0 }, { 0, 1, 0 }, { 0, 0, 0 } };
		int[][] edgeDetection1 ={ { 1, 0, -1 }, { 0, 0, 0 }, { -1, 0, 1 } };
		int[][] edgeDetection2 ={ { 0, -1, 0 }, { -1, 4, -1 }, { 0, -1, 0 } };
		int[][] edgeDetection3 ={ { -1, -1, -1 }, { -1, 8, -1 }, { -1, -1, -1 } };
		int[][] sharpen ={ { 0, -1, 0 }, { -1, 5, -1 }, { 0, -1, 0 } };
		int[][] boxBlur = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
		int[][] gaussian1 = { { 1, 2, 1 }, { 2, 4, 2 }, { 1, 2, 1 } };
		int[][] gaussian2 = { { 1, 4, 6, 4, 1 }, { 4, 16, 24, 16, 4 }, { 6, 24, 36, 24, 6 }, { 4, 16, 24, 16, 4 }, { 1, 4, 6, 4, 1 } };
		
		System.out.println("Kernels available: identity, edgeDetection1, edgeDetection2, edgeDetection3, sharpen, boxBlur, gaussian1, gaussian2");
		System.out.println("Go with kernel: ");
		String str= sc.nextLine();
		int[][] temp = (str=="gaussian2") ? new int[5][5] : new int [3][3];
		if (str.equals("identity")) System.arraycopy(identity, 0, temp, 0, identity.length);
		else if (str.equals("edgeDetection1")) System.arraycopy(edgeDetection1, 0, temp, 0, identity.length);
		else if (str.equals("edgeDetection2")) System.arraycopy(edgeDetection2, 0, temp, 0, identity.length);
		else if (str.equals("edgeDetection3")) System.arraycopy(edgeDetection3, 0, temp, 0, identity.length);
		else if (str.equals("sharpen")) System.arraycopy(sharpen, 0, temp, 0, identity.length);
		else if (str.equals("boxBlur")) System.arraycopy(boxBlur, 0, temp, 0, identity.length);
		else if (str.equals("gaussian1")) System.arraycopy(gaussian1, 0, temp, 0, identity.length);
		else if (str.equals("gaussian2")) System.arraycopy(gaussian2, 0, temp, 0, identity.length);
		else {
			System.out.println("Kernel not available.");
			return;
		}
		
		int i = 0;
		if (dir.isDirectory()) { // make sure it's a directory
			for (final File f : dir.listFiles(IMAGE_FILTER)) {   //read all images from directory originals
					i++;
					
					BufferedImage img = null;
					BufferedImage img2 = null;
					File output_file1 = null;
					File output_file2 = null;
					try {
						img = ImageIO.read(f);
						img2 = ImageIO.read(f);

						int[][] gaus = { { 1, 2, 1 }, { 2, 4, 2 }, { 1, 2, 1 } };

						output_file1 = new File("processed/seq" + i + ".jpg");
						long startSeq = System.currentTimeMillis();
						ImageIO.write(sequential(img, temp), "jpg", output_file1);
						long endSeq = System.currentTimeMillis();
						output_file2 = new File("processed/par" + i + ".jpg");
						long startPar = System.currentTimeMillis();
						ImageIO.write(parallel(img2, temp), "jpg", output_file2);
						long endPar = System.currentTimeMillis();
						System.out.println("Done with image: " + f.getName());
						System.out.println("Sequential: " + (endSeq - startSeq) + " Parallel: " + (endPar-startPar));
					} catch (final IOException e) {
						// handle errors here
					}
				}
			
		}
	}
	
	
	

	public static ArrayList<int[][]> transferToRGB(BufferedImage bufferedImage) {
		int width = bufferedImage.getWidth();
		int height = bufferedImage.getHeight();
		ArrayList<int[][]> rgb = new ArrayList<int[][]>();

		int[][] r = new int[width][height];
		int[][] g = new int[width][height];
		int[][] b = new int[width][height];

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				Color c = new Color(bufferedImage.getRGB(i, j));
				r[i][j] = c.getRed();
				g[i][j] = c.getGreen();
				b[i][j] = c.getBlue();
			}
		}
		rgb.add(r);
		rgb.add(g);
		rgb.add(b);
		return rgb;
	}

	public static int[][] process(int[][] imgArr, int[][] kernel) {
		int rows = imgArr.length;
		int columns = imgArr[0].length;
		int kSize = kernel.length;
		int kRadius = kSize / 2;
		int[][] newImg = new int[rows][columns];

		for (int i = rows - 1; i >= 0; i--) {
			for (int j = columns - 1; j >= 0; j--) {
				int sum = 0;
				for (int kRows = kSize - 1; kRows >= 0; kRows--) {
					for (int kCol = kSize - 1; kCol >= 0; kCol--) {
						sum += kernel[kRows][kCol]
								* imgArr[bound(i + kRows - kRadius, rows)][bound(j + kCol - kRadius, columns)];
					}
				}
				int sumMat = sumMat(kernel);
				if (sumMat > 1 || sumMat < -1)
					newImg[i][j] = (int) Math.ceil(sum / sumMat);
				else
					newImg[i][j] = sum;
			}
		}

		return newImg;
	}

	public static int[][] processFromTo(int[][] imgArr, int[][] kernel, int min, int max) {
		int rows = imgArr.length;
		int columns = imgArr[0].length;
		int kSize = kernel.length;
		int kRadius = kSize / 2;
		int[][] newImg = new int[rows][columns];

		for (int i = max - 1; i >= min; i--) {
			for (int j = columns - 1; j >= 0; j--) {
				int sum = 0;
				for (int kRows = kSize - 1; kRows >= 0; kRows--) {
					for (int kCol = kSize - 1; kCol >= 0; kCol--) {
						sum += kernel[kRows][kCol]
								* imgArr[bound(i + kRows - kRadius, max)][bound(j + kCol - kRadius, columns)];
					}
				}
				int sumMat = sumMat(kernel);
				if (sumMat > 1 || sumMat < -1)
					newImg[i][j] = (int) Math.ceil(sum / sumMat);
				else
					newImg[i][j] = sum;
			}
		}

		return newImg;
	}

	public static int[][] pickUp(int[][] processedArr, int min, int max) {
		int columns = processedArr[0].length;
		int[][] newImg = new int[max - min][columns];
		for (int i = 0; i < max - min; i++) {
			for (int j = 0; j < columns; j++) {
				newImg[i][j] = processedArr[i + min][j];
			}
		}
		return newImg;
	}

	public static int[][] processParallel(int[][] imgArr, int[][] kernel) {
		int NT = Runtime.getRuntime().availableProcessors(); // num of threads
		int rows = imgArr.length;
		int columns = imgArr[0].length;
		int part = (int) Math.ceil((double) rows / NT);
		int[][] newImg = new int[rows][columns];
		RunImg[] ths = new RunImg[NT];
		Thread[] threads = new Thread[NT];
		for (int i = 0; i < threads.length; i++) {
			ths[i] = new RunImg(newImg, imgArr, kernel, i * part, Math.min((i + 1) * part, rows));
			threads[i] = new Thread(ths[i]);
			threads[i].start();
		}
		try {
			for (int i = 0; i < threads.length; i++)
				threads[i].join();
		} catch (Exception e) {
		}
		int[][] temp = null;
		for (int i = 0; i < threads.length; i++) {
			temp = pickUp(ths[i].getMatrix(), i * part, Math.min((i + 1) * part, rows));
			System.arraycopy(temp, 0, newImg, i * part, temp.length);
		}

		return newImg;
	}
	
	public static BufferedImage sequential(BufferedImage img, int[][] kernel) {
		ArrayList<int[][]> rgbs = transferToRGB(img);

		ArrayList<int[][]> processedRgb = new ArrayList<int[][]>();
		processedRgb.add(process(rgbs.get(0), kernel));
		processedRgb.add(process(rgbs.get(1), kernel));
		processedRgb.add(process(rgbs.get(2), kernel));

		BufferedImage temp = img;
		BufferedImage newImg = backToImg(processedRgb, temp);
		return newImg;

	}
	
	public static BufferedImage sequentialMPI(ArrayList<int[][]>  rgbs, int[][] kernel) {
		ArrayList<int[][]> processedRgb = new ArrayList<int[][]>();
		processedRgb.add(process(rgbs.get(0), kernel));
		processedRgb.add(process(rgbs.get(1), kernel));
		processedRgb.add(process(rgbs.get(2), kernel));

		BufferedImage copyOfImage =
		        new BufferedImage(rgbs.get(0)[0].length, rgbs.get(0).length, BufferedImage.TYPE_INT_ARGB);
		BufferedImage newImg = backToImg(processedRgb, copyOfImage);
		return newImg;

	}

	public static BufferedImage parallel(BufferedImage img, int[][] kernel) {
		ArrayList<int[][]> rgbs = transferToRGB(img);

		ArrayList<int[][]> processedRgb = new ArrayList<int[][]>();
		processedRgb.add(processParallel(rgbs.get(0), kernel));
		processedRgb.add(processParallel(rgbs.get(1), kernel));
		processedRgb.add(processParallel(rgbs.get(2), kernel));

		BufferedImage temp = img;
		BufferedImage newImg = backToImg(processedRgb, temp);
		return newImg;
	}

	public static BufferedImage backToImg(ArrayList<int[][]> rgb, BufferedImage newImg) {
		int[][] r = rgb.get(0);
		int[][] g = rgb.get(1);
		int[][] b = rgb.get(2);

		for (int i = 0; i < r.length; i++) {
			for (int j = 0; j < r[0].length; j++) {
				int red = bound(r[i][j], 256); // if value <0 make white, if >256 make black
				int green = bound(g[i][j], 256);
				int blue = bound(b[i][j], 256);
				Color newRGB = new Color(red, green, blue);
				int value = newRGB.getRGB();
				newImg.setRGB(i, j, value);
			}
		}

		return newImg;
	}

	private static int bound(int value, int endIndex) {
		if (value < 0)
			return 0;
		if (value < endIndex)
			return value;
		return endIndex - 1;
	}

	public static int sumMat(int[][] mat) {
		int sum = 0;
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[0].length; j++) {
				sum += mat[i][j];
			}
		}
		return sum;
	}

	
}

class RunImg implements Runnable {

	int[][] imgArr, kernel, newImg;
	int min, max;

	RunImg(int[][] newImg, int[][] imgArr, int[][] kernel, int min, int max) {
		this.imgArr = imgArr;
		this.kernel = kernel;
		this.newImg = newImg;
		this.min = min;
		this.max = max;
	}

	public int[][] getMatrix() {
		return newImg;
	}

	@Override
	public void run() {
		this.newImg = Kernel_Image.processFromTo(imgArr, kernel, min, max);
	}

}
