package project;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;

import mpi.MPI;

import java.awt.Color;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.imageio.ImageIO;

public class MPJ_Kernel {
	
	static final File dir = new File("originals");
	static File dir2 = new File("processed");
	static final String[] EXTENSIONS = new String[] { "jpg", "png" };  // array of supported extensions 
	static final FilenameFilter IMAGE_FILTER = new FilenameFilter() {  // filter to identify images based on their extensions
		@Override
		public boolean accept(final File dir, final String name) {
			for (final String ext : EXTENSIONS) {
				if (name.endsWith("." + ext)) {
					return (true);
				}
			}
			return (false);
		}
	};
	
	//kernels
	static int[][] identity = { { 0, 0, 0 }, { 0, 1, 0 }, { 0, 0, 0 } };
	static int[][] edgeDetection1 ={ { 1, 0, -1 }, { 0, 0, 0 }, { -1, 0, 1 } };
	static int[][] edgeDetection2 ={ { 0, -1, 0 }, { -1, 4, -1 }, { 0, -1, 0 } };
	static int[][] edgeDetection3 ={ { -1, -1, -1 }, { -1, 8, -1 }, { -1, -1, -1 } };
	static int[][] sharpen ={ { 0, -1, 0 }, { -1, 5, -1 }, { 0, -1, 0 } };
	static int[][] boxBlur = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
	static int[][] gaussian1 = { { 1, 2, 1 }, { 2, 4, 2 }, { 1, 2, 1 } };
	static int[][] gaussian2 = { { 1, 4, 6, 4, 1 }, { 4, 16, 24, 16, 4 }, { 6, 24, 36, 24, 6 }, { 4, 16, 24, 16, 4 }, { 1, 4, 6, 4, 1 } };

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc= new Scanner(System.in);
		final File[] files = dir2.listFiles();
		for (int j = 0; j < files.length; j++) {
			files[j].delete();                     //empty folder of proccesed imgs before starting
		}
		File[] filess = dir.listFiles();
		int[][] temp ={ { 0, -1, 0 }, { -1, 5, -1 }, { 0, -1, 0 } };
		int i = 0;
		if (dir.isDirectory()) { // make sure it's a directory
			for (final File f : dir.listFiles(IMAGE_FILTER)) {   //read all images from directory originals
					i++;
					MPI.Init(args);
					int rank = MPI.COMM_WORLD.Rank();
					int tag = 100;
					int tag2 = 200;
					int dest;
					BufferedImage img = null;
					File output_file1 = null;
					
							try {
								img = ImageIO.read(f);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					if(rank==1) {   //sending data
						dest =0; //0 process			
							Object[] imageToSend = new Object[1];
							imageToSend[0] = Kernel_Image.transferToRGB(img);  //ArrayList<int[][]> rgb
							long start = System.currentTimeMillis();
							MPI.COMM_WORLD.Send(imageToSend, 0, 1, MPI.OBJECT, dest, tag);
							
							System.out.println("Process: "+rank+"");
							System.out.println("Image sent: " + f.getName());
							
							
							Object[] imageToRecv = new Object[1];
							MPI.COMM_WORLD.Recv(imageToRecv, 0, 1, MPI.OBJECT, dest, tag2);
							
							File received = (File) imageToRecv[0];
							System.out.println("Process- "+rank+"");
							System.out.println("Image processed received: " + received.getName());
							long end = System.currentTimeMillis();
					    	System.out.println("Time: " + (end - start));
							
												
						
					} 
					else {   //receiving data
						dest=1;
						Object[] imageToRecv = new Object[1];						
						MPI.COMM_WORLD.Recv(imageToRecv, 0, 1, MPI.OBJECT, dest, tag);  //receive
						//do some work
						ArrayList<int[][]> tempp = (ArrayList<int[][]>) imageToRecv[0];
						//System.out.println(tempp.get(0)[0][0]);
						BufferedImage backImg = Kernel_Image.backToImg(tempp, img);
						output_file1 = new File("processed/dist" + i + ".jpg");
						try {
							ImageIO.write(Kernel_Image.sequential(backImg, temp), "jpg", output_file1);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						System.out.println("Process- " +rank+"");
						System.out.println("Done with image "  + f.getName());
						
						//send back
						File[] processedImageToSend = new File[1];
						processedImageToSend[0] = output_file1;
						MPI.COMM_WORLD.Send(processedImageToSend, 0, 1, MPI.OBJECT, dest, tag2);
					}
					MPI.Finalize();
			}
		}	
		

	}

}
